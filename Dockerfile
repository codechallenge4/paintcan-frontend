# Use a imagem do Node.js com a versão desejada
FROM node

# Define o diretório de trabalho
WORKDIR /app

# Copia os arquivos de package.json e package-lock.json
COPY package*.json ./

# Instala as dependências
RUN npm install

# Copia o código do frontend
COPY . .

# Gera a build do Next.js
RUN npm run build

# Expõe a porta em que o frontend estará rodando (opcional, depende do uso)
EXPOSE 3000

# Inicia o aplicativo Next.js
CMD ["npm", "run", "start"]
