import HomePage from '../_components'
import { ThemeProvider } from "styled-components";
import { lightTheme, darkTheme } from '@/styles/themes'
import { useState } from 'react';
import { GlobalStyle } from '@/styles/globalStyles';
import { CheckboxLabel, Label, SwitcherCheckboxInput, SwitcherCheckboxLabel, SwitcherCheckboxWrapper, SwitcherContainer } from '@/_components/style';

export default function Home() {
  const [isDarkTheme, setIsDarkTheme] = useState(false);

  const toggleTheme = () => {
    setIsDarkTheme(!isDarkTheme);
  };

  return (
    <ThemeProvider theme={isDarkTheme ? darkTheme : lightTheme}>
      <GlobalStyle />
      <HomePage />
      <SwitcherContainer>
        <Label>Modo Norturno</Label>
        <SwitcherCheckboxWrapper>
          <SwitcherCheckboxInput
            type="checkbox"
            checked={isDarkTheme}
          />
          <SwitcherCheckboxLabel aria-checked={isDarkTheme} onClick={toggleTheme} />
        </SwitcherCheckboxWrapper>  
      </SwitcherContainer>
    </ThemeProvider>
  )
}
