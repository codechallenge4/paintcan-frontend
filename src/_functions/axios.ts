import axios, { AxiosResponse } from 'axios';

const base_url = 'http://localhost:8080/paint-can/'

// Função para calcular algo
export async function calculate(data: any): Promise<AxiosResponse> {
  const url = base_url + 'calculate';
  return await axios.post(url, data);
}

// Função para obter a quantidade de algo
export async function quantity(data: any): Promise<AxiosResponse> {
  const url = base_url + 'quantity';
  return await axios.post(url, data);
}

// Função para pintar uma lata de algo
export async function paintCan(data: any): Promise<AxiosResponse> {
  const url = base_url + 'paint-can';
  return await axios.post(url, data);
}