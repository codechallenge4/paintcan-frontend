

// Definição do tema light
export const lightTheme = {
    default: {
        backgroundColor: "#101010",
        textColor: "#000000",
    },
    card: {
      backgroundColor: "#FAF0E6",
      borderRadius: "8px",
      boxShadow: "4px 4px 6px rgba(0, 0, 0, 0.4)",
      padding: "16px",
      maxWidth: "300px",
      margin: "0 auto",
    },
    title: {
      fontSize: "24px",
      fontWeight: "bold",
      marginBottom: "8px",
    },
    content: {
      fontSize: "16px",
      color: "#444",
      marginBottom: "16px",
    },
    button: {
      backgroundColor: "#007bff",
      color: "#fff",
      border: "none",
      borderRadius: "4px",
      padding: "8px 16px",
      fontSize: "16px",
      cursor: "pointer",
      marginTop: "16px",
      marginLeft: "auto",
      marginRight: "auto",
      display: "block",
    },
    container: {
      boxShadow: "0 4px 6px rgba(0, 0, 0, 0.2)",
      maxWidth: "1200px",
      marginLeft: "auto",
      marginRight: "auto",
      paddingLeft: "16px",
      paddingRight: "16px",
    },
    box: {
      boxShadow: "0 4px 6px rgba(0, 0, 0, 0.2)",  
      backgroundColor: "#fff",
      border: "1px solid #ddd",
      borderRadius: "4px",
      padding: "16px",
    },
    loadingSpinner: {
        border: "4px solid #f3f3f3",
        borderTop: "4px solid #3182ce",
      },
      alertContainer: {
        backgroundColor: "rgba(0, 0, 0, 0.5)",
      },
      alertContent: {
        backgroundColor: "#fff",
        boxShadow: "0 2px 10px rgba(0, 0, 0, 0.1)",
      },
      headerWrapper: {
        textAlign: "center",
      },
      headerTitle: {
        fontSize: "24px",
        marginBottom: "16px",
      },
      description: {
        fontSize: "16px",
      },
      loadingWrapper: {
        position: "fixed",
        top: "0",
        left: "0",
        right: "0",
        bottom: "0",
        backgroundColor: "rgba(0, 0, 0, 0.5)",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        zIndex: "9999",
      },
      checkboxWrapper: {
        display: "flex",
        alignItems: "center",
      },
      checkboxLabel: {
        marginLeft: "8px",
      },
      checkboxInput: {
        marginRight: "8px",
      },
      row: {
        display: "flex",
        gap: "16px",
      },
      input: {
        padding: "8px",
        borderRadius: "4px",
        border: "1px solid #ccc",
        marginBottom: "16px",
        focus: {
          outline: "none",
          borderColor: "#3182ce",
        },
      },
      gridContainer: {
        display: "grid",
        gridTemplateColumns: "repeat(2, 1fr)",
        gap: "16px",
      },
      form: {
        display: "flex",
        flexDirection: "column",
      },
      label: {
        fontWeight: "bold",
        marginBottom: "8px",
      },
      switch: {
        backgroundColor: '#CBD5E0',
        trasnform: 'translateX(0)'
      }
  };
  
  // Definição do tema dark
  export const darkTheme = {
    backgroundColor: "#222222",
    textColor: "#ffffff",
    card: {
      backgroundColor: "#333",
      borderRadius: "8px",
      boxShadow: "0 4px 6px rgba(0, 0, 0, 0.2)",
      padding: "16px",
      maxWidth: "300px",
      margin: "0 auto",
    },
    title: {
      fontSize: "24px",
      fontWeight: "bold",
      marginBottom: "8px",
      color: "#fff",
    },
    content: {
      fontSize: "16px",
      color: "#ccc",
      marginBottom: "16px",
    },
    button: {
      backgroundColor: "#007bff",
      color: "#fff",
      border: "none",
      borderRadius: "4px",
      padding: "8px 16px",
      fontSize: "16px",
      cursor: "pointer",
      marginTop: "16px",
      marginLeft: "auto",
      marginRight: "auto",
      display: "block",
    },
    container: {
      maxWidth: "1200px",
      marginLeft: "auto",
      marginRight: "auto",
      paddingLeft: "16px",
      paddingRight: "16px",
    },
    box: {
      boxShadow: "0 4px 6px rgba(0, 0, 0, 0.2)",
      backgroundColor: "#444",
      border: "1px solid #666",
      borderRadius: "4px",
      padding: "16px",
    },
    loadingSpinner: {
        border: "4px solid #f3f3f3",
        borderTop: "4px solid #3182ce",
      },
      alertContainer: {
        backgroundColor: "rgba(0, 0, 0, 0.5)",
      },
      alertContent: {
        backgroundColor: "#121212",
        boxShadow: "0 2px 10px rgba(0, 0, 0, 0.1)",
      },
      headerWrapper: {
        textAlign: "center",
      },
      headerTitle: {
        fontSize: "24px",
        marginBottom: "16px",
      },
      description: {
        fontSize: "16px",
      },
      loadingWrapper: {
        position: "fixed",
        top: "0",
        left: "0",
        right: "0",
        bottom: "0",
        backgroundColor: "rgba(255, 255, 255, 0.5)",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        zIndex: "9999",
      },
      checkboxWrapper: {
        display: "flex",
        alignItems: "center",
      },
      checkboxLabel: {
        marginLeft: "8px",
      },
      checkboxInput: {
        marginRight: "8px",
      },
      row: {
        display: "flex",
        gap: "16px",
      },
      input: {
        padding: "8px",
        borderRadius: "4px",
        border: "1px solid #ddd",
        marginBottom: "16px",
        focus: {
          outline: "none",
          borderColor: "#90cdf4",
        },
      },
      gridContainer: {
        display: "grid",
        gridTemplateColumns: "repeat(2, 1fr)",
        gap: "16px",
      },
      form: {
        display: "flex",
        flexDirection: "column",
      },
      label: {
        fontWeight: "bold",
        marginBottom: "8px",
      },
      switch: {
        backgroundColor: '#38B2AC',
        trasnform: 'translateX(24px)'
      }
      
  };