import { DefaultTheme, createGlobalStyle } from 'styled-components';
interface MyTheme extends DefaultTheme {
    backgroundColor: string;
    textColor: string;
    // Adicione outras propriedades de estilo do tema aqui
  }

export const GlobalStyle = createGlobalStyle<{ theme: MyTheme }>`
body {
    background-color: ${(props) => props.theme.backgroundColor};
    color: ${(props) => props.theme.textColor};
    // Adicione outros estilos globais aqui
}
`;