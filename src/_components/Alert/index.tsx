import { ReactComponentElement } from "react";
import { AlertContainer, AlertContent, Button } from "../style";

export default function Alert ({ text, setShowAlert } : {text: any, setShowAlert:any }) {
    
    return (
      <AlertContainer>
        <AlertContent>
            <h2>Alerta!</h2>
            <p>{text}</p>
            <Button onClick={() => setShowAlert(false)}>Fechar</Button>
        </AlertContent>
      </AlertContainer>
    );
  };
  