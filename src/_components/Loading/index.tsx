import { LoadingSpinner, LoadingWrapper } from "../style";

export default function LoadingComponent () {
    return (
      <LoadingWrapper>
        <LoadingSpinner />
      </LoadingWrapper>
    );
}