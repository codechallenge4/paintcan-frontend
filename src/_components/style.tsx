import styled from "styled-components";

// Componente de Card com estilos do tema
export const Card = styled.div`
  background-color: ${(props) => props.theme.card.backgroundColor};
  border-radius: ${(props) => props.theme.card.borderRadius};
  box-shadow: ${(props) => props.theme.card.boxShadow};
  padding: ${(props) => props.theme.card.padding};
  max-width: ${(props) => props.theme.card.maxWidth};
  margin: ${(props) => props.theme.card.margin};
`;

// Estilos do Title
export const Title = styled.h1`
  font-size: ${(props) => props.theme.title.fontSize};
  font-weight: ${(props) => props.theme.title.fontWeight};
  margin-bottom: ${(props) => props.theme.title.marginBottom};
  color: ${(props) => props.theme.content.color};
`;

// Estilos do Content
export const Content = styled.p`
  font-size: ${(props) => props.theme.content.fontSize};
  color: ${(props) => props.theme.content.color};
  margin-bottom: ${(props) => props.theme.content.marginBottom};
`;

// Estilos do Button
export const Button = styled.button`
  background-color: ${(props) => props.theme.button.backgroundColor};
  color: ${(props) => props.theme.button.color};
  border: ${(props) => props.theme.button.border};
  border-radius: ${(props) => props.theme.button.borderRadius};
  padding: ${(props) => props.theme.button.padding};
  font-size: ${(props) => props.theme.button.fontSize};
  cursor: ${(props) => props.theme.button.cursor};
  margin-top: ${(props) => props.theme.button.marginTop};
  margin-left: ${(props) => props.theme.button.marginLeft};
  margin-right: ${(props) => props.theme.button.marginRight};
  display: ${(props) => props.theme.button.display};
`;

// Componente de Container com estilos do tema
export const Container = styled.div`
  max-width: ${(props) => props.theme.container.maxWidth};
  margin-left: ${(props) => props.theme.container.marginLeft};
  margin-right: ${(props) => props.theme.container.marginRight};
  padding-left: ${(props) => props.theme.container.paddingLeft};
  padding-right: ${(props) => props.theme.container.paddingRight};
`;

// Componente de Box com estilos do tema
export const Box = styled.div`
  background-color: ${(props) => props.theme.box.backgroundColor};
  border: ${(props) => props.theme.box.border};
  border-radius: ${(props) => props.theme.box.borderRadius};
  padding: ${(props) => props.theme.box.padding};
`;

// Componente de GridContainer com estilos do tema
export const GridContainer = styled.div`
  display: ${(props) => props.theme.gridContainer.display};
  grid-template-columns: ${(props) => props.theme.gridContainer.gridTemplateColumns};
  gap: ${(props) => props.theme.gridContainer.gap};
`;

// Componente de Form com estilos do tema
export const Form = styled.form`
  display: ${(props) => props.theme.form.display};
  flex-direction: ${(props) => props.theme.form.flexDirection};
`;

// Componente de Label com estilos do tema
export const Label = styled.label`
  font-weight: ${(props) => props.theme.label.fontWeight};
  margin-bottom: ${(props) => props.theme.label.marginBottom};
`;

// Componente de Input com estilos do tema
export const Input = styled.input`
  padding: ${(props) => props.theme.input.padding};
  border-radius: ${(props) => props.theme.input.borderRadius};
  border: ${(props) => props.theme.input.border};
  margin-bottom: ${(props) => props.theme.input.marginBottom};
  &:focus {
    outline: ${(props) => props.theme.input.focus.outline};
    border-color: ${(props) => props.theme.input.focus.borderColor};
  }
`;

// Componente CheckboxWrapper com estilos do tema
export const CheckboxWrapper = styled.div`
  display: ${(props) => props.theme.checkboxWrapper.display};
  align-items: ${(props) => props.theme.checkboxWrapper.alignItems};
`;

// Componente CheckboxLabel com estilos do tema
export const CheckboxLabel = styled.label`
  margin-left: ${(props) => props.theme.checkboxLabel.marginLeft};
`;

// Componente CheckboxInput com estilos do tema
export const CheckboxInput = styled.input`
  margin-right: ${(props) => props.theme.checkboxInput.marginRight};
`;

// Componente Row com estilos do tema
export const Row = styled.div`
  display: ${(props) => props.theme.row.display};
  gap: ${(props) => props.theme.row.gap};
`;

export const LoadingWrapper = styled.div`
  position: ${(props) => props.theme.loadingWrapper.position};
  top: ${(props) => props.theme.loadingWrapper.top};
  left: ${(props) => props.theme.loadingWrapper.left};
  right: ${(props) => props.theme.loadingWrapper.right};
  bottom: ${(props) => props.theme.loadingWrapper.bottom};
  background-color: ${(props) =>
    props.theme.loadingWrapper.backgroundColor};
  display: ${(props) => props.theme.loadingWrapper.display};
  justify-content: ${(props) =>
    props.theme.loadingWrapper.justifyContent};
  align-items: ${(props) => props.theme.loadingWrapper.alignItems};
  z-index: ${(props) => props.theme.loadingWrapper.zIndex};
`;
export const LoadingSpinner = styled.div`
  border: ${(props) => props.theme.loadingSpinner.border};
  border-top: ${(props) => props.theme.loadingSpinner.borderTop};
  border-radius: 50%;
  width: 40px;
  height: 40px;
  animation: spin 0.8s linear infinite;
  @keyframes spin {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`;

// Componente AlertContainer com estilos do tema
export const AlertContainer = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: ${(props) => props.theme.alertContainer.backgroundColor};
  color: ${(props) => props.theme.content.color};
  z-index: 9999;
`;

// Componente AlertContent com estilos do tema
export const AlertContent = styled.div`
  background-color: ${(props) => props.theme.alertContent.backgroundColor};
  padding: 24px;
  color: ${(props) => props.theme.content.color};
  border-radius: 4px;
  box-shadow: ${(props) => props.theme.alertContent.boxShadow};
`;

// Componente HeaderWrapper com estilos do tema
export const HeaderWrapper = styled.header`
  text-align: ${(props) => props.theme.headerWrapper.textAlign};
`;

// Componente HeaderTitle com estilos do tema
export const HeaderTitle = styled.h1`
  font-size: ${(props) => props.theme.headerTitle.fontSize};
  margin-bottom: ${(props) => props.theme.headerTitle.marginBottom};
  color: ${(props) => props.theme.content.color};
`;

// Componente Description com estilos do tema
export const Description = styled.p`
  font-size: ${(props) => props.theme.description.fontSize};
  color: ${(props) => props.theme.content.color};
`;

export const SwitcherCheckboxWrapper = styled.div`
  position: relative;
  margin-left: 10px;
  width: 48px;
  height: 24px;
  display: flex;
  align-items: center;
  
`;

export const SwitcherCheckboxInput = styled.input`
  opacity: 0;
  width: 0;
  height: 0;
`;

export const SwitcherCheckboxLabel = styled.label`
  display: flex;
  position: absolute;
  top: 0;
  left: 0;
  width: 48px;
  height: 24px;
  background-color: ${(props) => props.theme.switch.backgroundColor};
  border-radius: 9999px;
  cursor: pointer;
  transition: background-color 0.2s ease-in-out;

  &::after {
    content: '';
    display: block;
    position: absolute;
    top: 4px;
    left: 4px;
    width: 16px;
    height: 16px;
    background-color: #fff;
    border-radius: 50%;
    transition: transform 0.2s ease-in-out;
    transform: ${(props) => props.theme.switch.trasnform};
  }
`;

export const SwitcherContainer = styled.div`
  display: flex; 
  margin-top: 20px;
  align-items: center; 
  justify-content: center;
`