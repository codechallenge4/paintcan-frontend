import { Description, HeaderWrapper, HeaderTitle } from "../style";

export default function Header() {
    return (
        <HeaderWrapper>
            <HeaderTitle>Bem-vindo ao Calculadora de Tinta</HeaderTitle>
            <Description>Calcule a quantidade de tinta necessária para sua pintura</Description>
      </HeaderWrapper>
      );
}