import { useState } from "react";
import Wall from "./Wall";
import { Box, Button, Card, Container, Content, Form, GridContainer, Title } from "./style";
import LoadingComponent from "./Loading";
import { useForm } from "react-hook-form";
import { calculate, paintCan, quantity } from "@/_functions/axios";
import Alert from "./Alert";
import Header from "./Header";


export default function HomePage() {
    const [loading, setLoading] = useState(false)
    const { register, handleSubmit, formState: { errors }, setValue } = useForm();
    const [showAlert, setShowAlert] = useState(false);
    const [text, setText] = useState('')
    
    const onSubmit = async (data: any) => {
        setLoading(true)
        const formData = data.wall.map((element:any) => {
            return {
                height: Math.ceil(parseFloat(element.height.replace(',', '.')) * 100.0), 
                width: Math.ceil(parseFloat(element.width.replace(',', '.')) * 100.0), 
                doors: Number(element.doors), 
                windows: Number(element.windows)
            }
        });

        try {
            const responseCalculate = await calculate(formData)
    
            const responseQuantity = await quantity(responseCalculate.data)
    
            const responseLitters = await paintCan(responseQuantity.data)
    
            setLoading(false)
            setText('As latas necessárias para sua pintura são as: ' + responseLitters.data.paint_cans.join('L, ') + 'L')
            setShowAlert(true)    
        } catch (error: any) {
            setLoading(false)
            setText(error.response.data.message)
            setShowAlert(true)
        }
        
    }

    return (
        <Container style={{marginTop: '5%'}}>
            <Box>
                <Header />
                {loading && <LoadingComponent />}
                <Form method="POST" onSubmit={handleSubmit(onSubmit)}>
                    <GridContainer>
                            <Wall setValue={setValue} errors={errors} register={register} wallNumber={0}/>
                            <Wall setValue={setValue} errors={errors} register={register} wallNumber={1}/>
                            <Wall setValue={setValue} errors={errors} register={register} wallNumber={2}/>
                            <Wall setValue={setValue} errors={errors} register={register} wallNumber={3}/>
                    </GridContainer>
                    <Button>Calcular</Button>
                </Form>
            </Box>
            {showAlert && <Alert text={text} setShowAlert={setShowAlert} /> }
        </Container>
      );
}