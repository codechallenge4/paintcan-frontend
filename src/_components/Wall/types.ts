import { FieldValues, UseFormRegister } from "react-hook-form"

export interface Props {
    wallNumber : number
    register: UseFormRegister<FieldValues>
    errors: any,
    setValue: any
}