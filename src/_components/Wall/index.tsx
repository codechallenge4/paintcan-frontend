import { useState } from "react";
import { Card, CheckboxInput, CheckboxLabel, CheckboxWrapper, Content, Form, Input, Label, Row, Title } from "../style";
import { Props } from "./types";

export default function Wall({wallNumber, register, errors, setValue}: Props) {
    const [showDoors, setShowDoors] = useState(false)
    const [showWindows, setShowWindows] = useState(false)

    const handleCheckboxChangeDoor = (e: any) => {
        if(!e.target.checked) setValue(`wall.${wallNumber}.doors`, 0)
        setShowDoors(e.target.checked);
      };

    const handleCheckBoxChangeWindow = (e: any) => {
        if(!e.target.checked) setValue(`wall.${wallNumber}.windows`, 0)
        setShowWindows(e.target.checked)
    }

    const handleInputChange = (event: any) => {
        let inputValue = event.target.value;
        const regex = /^[1-9],[0-9]{0,2}$/;
      
        if (!regex.test(inputValue)) {
          inputValue = inputValue.replace(/[^0-9]/g, '');
          
          if (inputValue.length > 1) {
            inputValue = inputValue.substring(0, 1) + ',' + inputValue.substring(1);
          }
        }
      
        event.target.value = inputValue;
      };

    return (
        <Card key={wallNumber}>
            <Title style={{textAlign: 'center'}}>Parede {wallNumber + 1}</Title>
            <Content>
                
                    <Row>
                        <Label>Altura:</Label>
                        <Input 
                            type="text" 
                            {...register(`wall.${wallNumber}.height`, {required: "A altura da parede não pode ser vazia"})} 
                            placeholder="Digite a Altura"
                            maxLength={4}
                            onChange={handleInputChange} />
                    </Row>
                    <Row>
                        <Label>Largura:</Label>
                        <Input 
                            type="text" 
                            {...register(`wall.${wallNumber}.width`, {required: true})} 
                            placeholder="Digite a Largura" 
                            maxLength={4}
                            onChange={handleInputChange} />
                    </Row>
                    <Row style={{margin: 5}}>
                        <CheckboxWrapper>
                            <CheckboxInput 
                                id={`chk_doors-${wallNumber}`} 
                                onChange={handleCheckboxChangeDoor} 
                                type="checkbox" />
                            <CheckboxLabel htmlFor={`chk_doors-${wallNumber}`}>Possuirá portas</CheckboxLabel>
                        </CheckboxWrapper>
                        <Input 
                            style={{display: showDoors ? 'inline' : 'none'}} 
                            {...register(`wall.${wallNumber}.doors`)} 
                            type="text" 
                            placeholder="Quantidade" 
                            onChange={handleInputChange} />
                    </Row>
                    <Row style={{margin: 5}}>
                        <CheckboxWrapper>
                            <CheckboxInput 
                                id={`chk_windows-${wallNumber}`} 
                                onChange={handleCheckBoxChangeWindow} 
                                type="checkbox" />
                            <CheckboxLabel htmlFor={`chk_windows-${wallNumber}`}>Possuirá janelas</CheckboxLabel>
                        </CheckboxWrapper>
                        <Input 
                            style={{display: showWindows ? 'inline' : 'none'}} 
                            {...register(`wall.${wallNumber}.windows`)} 
                            type="text" 
                            placeholder="Quantidade" 
                            onChange={handleInputChange}/>
                    </Row>
                
            </Content>
        </Card>
    )
}