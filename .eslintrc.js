module.exports = {
    root: true,
    parser: "@babel/eslint-parser",
    extends: [
      "plugin:react/recommended",
      "plugin:@next/next/recommended",
      "plugin:prettier/recommended",
    ],
    plugins: ["react", "@next/next", "prettier"],
    rules: {
      // Adicione aqui as regras de linting que desejar
    },
  };